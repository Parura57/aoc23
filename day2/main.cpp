#include<stdio.h>
#include<fstream>
#include<iostream>
#include<string>
#include<vector>

int part1(int argc, char* argv[]) {
    std::ifstream fd(argv[1]);
    std::string line;
    int id;
    int result = 0;
    bool cont = true;

    while (getline(fd, line)) {
        cont = true;
        for (int i = 0; i < line.size(); i++) {
            if (!isdigit(line[i])) continue;
            if (!cont) break;

            if (line[i+2] == 'r' || (line[i+3] == 'r' && line[i+2] != 'g')) {
                                            // Man do i ever love the question specifying >= but expecting >
                if (std::stoi(line.substr(i, 2)) > 12) cont = false;
                i++;
            }
            
            else if (line[i+2] == 'g' || line[i+3] == 'g') {
                if (std::stoi(line.substr(i, 2)) > 13) cont = false;
                i++;
            }

            else if (line[i+2] == 'b' || line[i+3] == 'b') {
                if (std::stoi(line.substr(i, 2)) > 14) cont = false;
                i++;
            }

            else {
                id = std::stoi(line.substr(i, 3));
                i++;
            }
        }
        if (cont) { std::cout << "line " << line << std::endl; result += id; }
    }

    fd.close();
    return result;
}

int part2(int argc, char* argv[]) {
    std::ifstream fd(argv[1]);
    std::string line;
    int id, r, g, b;
    int result = 0;

    while (getline(fd, line)) {
        r = 0;
        g = 0;
        b = 0;

        for (int i = 0; i < line.size(); i++) {
            if (!isdigit(line[i])) continue;

            if (line[i+2] == 'r' || (line[i+3] == 'r' && line[i+2] != 'g')) {
                if (std::stoi(line.substr(i, 2)) > r) r = std::stoi(line.substr(i, 2));
                std::cout << line << " r " << r << " c " << line[i] << std::endl;
                i++;
            }
            
            else if (line[i+2] == 'g' || line[i+3] == 'g') {
                if (std::stoi(line.substr(i, 2)) > g) g = std::stoi(line.substr(i, 2));
                std::cout << line << " g " << g << " c " << line[i] << std::endl;
                i++;
            }

            else if (line[i+2] == 'b' || line[i+3] == 'b') {
                if (std::stoi(line.substr(i, 2)) > b) b = std::stoi(line.substr(i, 2));
                std::cout << line << " b " << b << " c " << line[i] << std::endl;
                i++;
            }
        }
        result += r*g*b;
    }

    fd.close();
    return result;
}

int main(int argc, char* argv[]) {
    //std::cout << part1(argc, argv) << std::endl;
    std::cout << part2(argc, argv) << std::endl;
    return 0;
}
