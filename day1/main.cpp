#include<stdio.h>
#include<fstream>
#include<iostream>
#include<string>
#include<vector>

int part1(int argc, char* argv[]) {
    std::ifstream fd(argv[1]);
    std::string line;
    int start, end, result = 0;

    while (getline(fd, line)) {
        for (int i = 0; i < line.size(); i++) {
            if (isdigit(line[i])) { start = line[i] - 0x30; break; }
        }
        for (int i = 0; i < line.size(); i++) {
            if (isdigit(line[i])) end = line[i] - 0x30;
        }
        printf("reading %i %i to %i\n", start, end, start*10+end);
        result += start*10 + end;
    }

    fd.close();
    return result;
}

int part2(int argc, char* argv[]) {
    std::ifstream fd(argv[1]);
    std::string line;
    int start, end, result = 0;
    std::vector<std::string> numbers = {"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};
    bool cont;

    while (getline(fd, line)) {
        cont = true;
        for (int i = 0; i < line.size(); i++) {
            if (!cont) break;
            for (int j = 0; j < numbers.size(); j++) {
                if (line.substr(i, numbers[j].size()) == numbers[j]) {
                    //std::cout << "identified start " << line.substr(i, numbers[j].size()) << " as " << j << std::endl;
                    start = j;
                    cont = false;
                    break;
                }
                else if (isdigit(line[i])) { start = line[i] - 0x30; cont = false; break; }
            }
        }
        for (int i = 0; i < line.size(); i++) {
            for (int j = 0; j < numbers.size(); j++) {
                if (line.substr(i, numbers[j].size()) == numbers[j]) {
                    //std::cout << "identified end " << line.substr(i, numbers[j].size()) << " as " << j << std::endl;
                    end = j;
                }
                else if (isdigit(line[i])) end = line[i] - 0x30;
            }
        }
        printf("reading %i %i to %i\n", start, end, start*10+end);
        result += start*10 + end;
    }

    fd.close();
    return result;
}

int main(int argc, char* argv[]) {
    //std::cout << part1(argc, argv);
    std::cout << part2(argc, argv);
    return 0;
}
